package net.WAC.wurmunlimited.mods.altars;

import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.ItemTemplatesCreatedListener;
import org.gotti.wurmunlimited.modloader.interfaces.Versioned;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;

import java.util.Properties;
import java.util.logging.Logger;

public class Altars implements WurmServerMod, Configurable, ItemTemplatesCreatedListener, Versioned {
    public static final Logger logger = Logger.getLogger(Altars.class.getName());
    public static final String version = "ty1.0";

    @Override
    public void configure(Properties properties) {
        AltarItems.marbleAltarSizeMultiplier = Float.parseFloat(properties.getProperty("Huge_Marble_Altar_Size_Multiplier", String.valueOf(AltarItems.marbleAltarSizeMultiplier)));
        AltarItems.sacrificialAltarSizeMultiplier = Float.parseFloat(properties.getProperty("Sacrificial_Altar_Size_Multiplier", String.valueOf(AltarItems.sacrificialAltarSizeMultiplier)));
    }

    @Override
    public void onItemTemplatesCreated() {
         AltarItems.register();
    }

    @Override
    public String getVersion() {
        return version;
    }
}
