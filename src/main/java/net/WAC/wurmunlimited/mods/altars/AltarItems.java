package net.WAC.wurmunlimited.mods.altars;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.TimeConstants;
import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.IconConstants;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;

import java.io.IOException;
import java.util.logging.Level;

public class AltarItems implements WurmServerMod, ItemTypes, MiscConstants {
    public static float marbleAltarSizeMultiplier = 1.0F;
    public static float sacrificialAltarSizeMultiplier = 1.0F;

    public static void register() {
        int hugeMarbleAltarTemplateId = (int)MiscConstants.NOID;
        int sacrificialAltarTemplateId = (int)MiscConstants.NOID;

        try {
            ItemTemplateBuilder builder = new ItemTemplateBuilder("jubaroo.altars.marble")
                .size(ItemSizes.ITEM_SIZE_HUGE)
                .name("marble altar", "altars", "A huge marble altar with beautiful hand carved designs all around it.")
                        .descriptions("almost full", "somewhat occupied", "half-full", "emptyish")
                        .itemTypes(new short[]{
                                ITEM_TYPE_NAMED,
                                ITEM_TYPE_HOLLOW,
                                ITEM_TYPE_NOTAKE,
                                ITEM_TYPE_STONE,
                                ITEM_TYPE_TURNABLE,
                                ITEM_TYPE_DECORATION,
                                ITEM_TYPE_REPAIRABLE,
                                ITEM_TYPE_HASDATA,
                                ITEM_TYPE_DOMAIN,
                                ITEM_TYPE_USE_GROUND_ONLY,
                                ITEM_TYPE_OWNER_DESTROYABLE,
                                ITEM_TYPE_HOLLOW_VIEWABLE
                        })
                .imageNumber((short)IconConstants.ICON_NONE)
                .behaviourType(BehaviourList.domainItemBehaviour)
                .combatDamage(0)
                .decayTime(TimeConstants.DECAYTIME_STONE)
                .primarySkill((int)MiscConstants.NOID)
                .bodySpaces(MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY)
                .modelName("model.decoration.altar.huge.marble.")
                .difficulty(50.0F)
                .weightGrams(460000)
                .material(Materials.MATERIAL_MARBLE)
                .weightGrams(5000)
                .dimensions((int)(41 * sacrificialAltarSizeMultiplier), (int)(41 * sacrificialAltarSizeMultiplier), (int)(210 * sacrificialAltarSizeMultiplier));
            ItemTemplate marbleAltar = builder.build();
            hugeMarbleAltarTemplateId = marbleAltar.getTemplateId();
        } catch (IOException e) {
            Altars.logger.log(Level.SEVERE, "Failed while creating template.", e);
        }
        if(hugeMarbleAltarTemplateId != MiscConstants.NOID) {
            AdvancedCreationEntry altarHuge = CreationEntryCreator.createAdvancedEntry(SkillList.MASONRY, ItemList.marbleSlab, ItemList.marbleBrick, hugeMarbleAltarTemplateId, false, false, 0.0f, true, true, CreationCategories.ALTAR);
            altarHuge.addRequirement(new CreationRequirement(1, ItemList.marbleSlab,     5, true));
            altarHuge.addRequirement(new CreationRequirement(2, ItemList.marbleBrick,    19, true));
            altarHuge.addRequirement(new CreationRequirement(3, ItemList.mortar,         10, true));
            altarHuge.addRequirement(new CreationRequirement(4, ItemList.woad,           5,  true));
            altarHuge.addRequirement(new CreationRequirement(5, ItemList.cochineal,      5,  true));
            altarHuge.addRequirement(new CreationRequirement(6, ItemList.plank,          4,  true));
            altarHuge.addRequirement(new CreationRequirement(7, ItemList.sandstoneBrick, 10, true));
        }


        try {
            ItemTemplateBuilder builder = new ItemTemplateBuilder("jubaroo.altars.sacrificial")
                .name("sacrificial altar", "sacrificial altars", "A stone altar with foreign markings and dark energy. It is mainly used by cultists for rituals and sacrificing.")
                .descriptions("almost full", "somewhat occupied", "half-full", "emptyish")
                .itemTypes(new short[]{
                        ITEM_TYPE_NAMED,
                        ITEM_TYPE_HOLLOW,
                        ITEM_TYPE_NOTAKE,
                        ITEM_TYPE_STONE,
                        ITEM_TYPE_TURNABLE,
                        ITEM_TYPE_DECORATION,
                        ITEM_TYPE_REPAIRABLE,
                        ITEM_TYPE_HASDATA,
                        ITEM_TYPE_DOMAIN,
                        ITEM_TYPE_USE_GROUND_ONLY,
                        ITEM_TYPE_OWNER_DESTROYABLE,
                        ITEM_TYPE_HOLLOW_VIEWABLE
                })
                .imageNumber((short)IconConstants.ICON_NONE)
                .behaviourType(BehaviourList.domainItemBehaviour)
                .combatDamage(0)
                .decayTime(TimeConstants.DECAYTIME_STONE)
                .primarySkill((int) MiscConstants.NOID)
                .bodySpaces(MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY)
                .modelName("model.decoration.altar.sacrificial.")
                .difficulty(35.0F)
                .weightGrams(290000)
                .material(Materials.MATERIAL_STONE)
                .value(1000)
                .dimensions((int)(41 * sacrificialAltarSizeMultiplier), (int)(41 * sacrificialAltarSizeMultiplier), (int)(210 * sacrificialAltarSizeMultiplier));
            ItemTemplate sacrificialAltar = builder.build();
            sacrificialAltarTemplateId = sacrificialAltar.getTemplateId();
        } catch (IOException e) {
            Altars.logger.log(Level.SEVERE, "Failed while creating template.", e);
        }
        if(sacrificialAltarTemplateId != MiscConstants.NOID) {
            AdvancedCreationEntry altarSacrificial = CreationEntryCreator.createAdvancedEntry(SkillList.MASONRY, ItemList.stoneSlab, ItemList.stoneBrick, sacrificialAltarTemplateId, false, false, 0.0f, true, true, CreationCategories.ALTAR);
            altarSacrificial.addRequirement(new CreationRequirement(1, ItemList.stoneSlab,  4,  true));
            altarSacrificial.addRequirement(new CreationRequirement(2, ItemList.stoneBrick, 9,  true));
            altarSacrificial.addRequirement(new CreationRequirement(3, ItemList.mortar,     6,  true));
            altarSacrificial.addRequirement(new CreationRequirement(4, ItemList.cochineal,  15, true));
            altarSacrificial.addRequirement(new CreationRequirement(5, ItemList.ironBar,    10, true));
        }
    }
}
